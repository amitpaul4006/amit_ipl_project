const csv = require("csvtojson");
const matches_file_path         = "./src/data/matches.csv";
const matches_file_path2        = "./src/data/deliveries.csv";
const matchesPlayedPerYear      = require("./matchesPlayedPerYear");
const matchesOwnPerTeamPerYear  = require("./matchesOwnPerTeamPerYear");
const ExtraRunPerTeam           = require("./ExtraRunPerTeam");
const economyOfTop10Bowlers     = require("./economyOfTop10Bowlers");

const JSON_matchesPlayedPerYear_FILE_PATH       = "./src/public/output/matchesPlayedPerYear.json"
const JSON_matchesOwnPerTeamPerYear_FILE_PATH   = "./src/public/output/matchesOwnPerTeamPerYear.json"
const JSON_ExtraRunPerTeam_FILE_PATH            = "./src/public/output/ExtraRunPerTeam.json"
const JSON_economyOfTop10Bowlers_FILE_PATH      = "./src/public/output/economyOfTop10Bowlers .json"

const fs = require("fs");

function main() {
    csv()
        .fromFile(matches_file_path)
        .then((matches) => {
            csv()
                .fromFile(matches_file_path2)
                .then((deliveries) => {
                    let result1 = matchesPlayedPerYear(matches);
                    let result2 = matchesOwnPerTeamPerYear(matches);
                    let result3 = ExtraRunPerTeam(deliveries, matches);
                    let result4 = economyOfTop10Bowlers(deliveries,matches);
                    console.log(result4);

                    saveData(result1, result2, result3, result4);

                });

        });
}

main();

function saveData(result1, result2, result3, result4) {
    const jsonData = {
        matchesPlayedPerYear    : result1,
        matchesOwnPerTeamPerYear: result2,
        ExtraRunPerTeam         : result3,
        economyOfTop10Bowlers   : result4
    };
    const jsonString = JSON.stringify(jsonData.matchesPlayedPerYear);
    const jsonString1 = JSON.stringify(jsonData.matchesOwnPerTeamPerYear);
    const jsonString2 = JSON.stringify(jsonData.ExtraRunPerTeam);
    const jsonString3 = JSON.stringify(jsonData.economyOfTop10Bowlers);


    fs.writeFile(JSON_matchesPlayedPerYear_FILE_PATH, jsonString, "utf8", err => {
        if (err) {
            console.error(err);
        }

    });
    fs.writeFile(JSON_matchesOwnPerTeamPerYear_FILE_PATH, jsonString1, "utf8", err => {
        if (err) {
            console.error(err);
        }

    });
    fs.writeFile(JSON_ExtraRunPerTeam_FILE_PATH, jsonString2, "utf8", err => {
        if (err) {
            console.error(err);
        }

    });
    fs.writeFile(JSON_economyOfTop10Bowlers_FILE_PATH, jsonString3, "utf8", err => {
        if (err) {
            console.error(err);
        }

    });
}



