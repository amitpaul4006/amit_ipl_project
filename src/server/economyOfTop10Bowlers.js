function economyOfTop10Bowlers(deliveries, matches) {
    const matchId = matches.reduce((res_arr, match) => {
        let season = match.season;
        if (season == 2015) {
            res_arr.push(match.id)
        }
        return res_arr;
    }, []);
    const result4 = {};
    const total_balls = deliveries.reduce((acc, bowler_var) => {
        let All_bowler = bowler_var.bowler;
        let id_deli = bowler_var.match_id;
        if (matchId.includes(id_deli)) {
            if (acc[All_bowler]) {
                acc[All_bowler] += 1;

            }
            else {
                acc[All_bowler] = 1;
            }
        }
        return acc;

    }, []);
    const total_run = deliveries.reduce((acc, bowler_var) => {
        let All_bowler = bowler_var.bowler;
        let id_deli = bowler_var.match_id;
        let totalRun = bowler_var.total_runs
        if (matchId.includes(id_deli)) {
            if (acc[All_bowler]) {
                acc[All_bowler] += parseInt(totalRun);

            }
            else {
                acc[All_bowler] = parseInt(totalRun);
            }
        }
        return acc;

    }, []);

    for (let data in total_balls) {
        total_balls[data] = parseFloat((total_balls[data] / 6).toFixed(2));
    }
    for (let data in total_run) {
        total_run[data] = parseFloat((total_run[data] / total_balls[data]).toFixed(2));
    }
    let arr_obj = []
    for (let data in total_run) {
        arr_obj.push([data, total_run[data]]);
    }
    arr_obj.sort(function (a, b) {
        return a[1] - b[1];
    })
    arr_obj = arr_obj.slice(0, 10);

    for (let data of arr_obj) {
        let name = data[0];
        let value = data[1];
        result4[name] = parseFloat(value);
    }

    return result4
}
module.exports = economyOfTop10Bowlers;