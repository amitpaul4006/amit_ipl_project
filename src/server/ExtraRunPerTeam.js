function ExtraRunPerTeam(deliveries, matches) {
    const matchId = matches.reduce((res_arr, match) => {
        let season = match.season;
        if (season == 2016) {
            res_arr.push(match.id)
        }
        return res_arr;
    }, []);
    // const result3 = {};
    const resObj = deliveries.reduce((result3, teams) => {
        let team = teams.bowling_team;
        let extra = teams.extra_runs;
        let id_deli = teams.match_id;
        if (matchId.includes(id_deli)) {
            if (result3[team] != undefined) {
                result3[team] += parseInt(extra);

            }
            else {
                result3[team] = parseInt(extra);
            }

        }
        return result3

    }, {});

    return resObj;
}
module.exports = ExtraRunPerTeam;