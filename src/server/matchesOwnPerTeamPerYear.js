function matchesOwnPerTeamPerYear(matches) {
    const resObj = matches.reduce((result2, match) => {
        let years = match.season;
        let win_team = match.winner;
        if (result2[years] === undefined) {
            result2[years] = {};
        }
        else {
            if (result2[years][win_team] != undefined) {
                result2[years][win_team] += 1;

            }
            else {
                result2[years][win_team] = 1;
            }

        }
        return result2;

    }, {});

    return resObj;
}

module.exports = matchesOwnPerTeamPerYear;